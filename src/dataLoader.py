#!/usr/bin/env python

# File: dataLoader.py
# Created by Claude Goubet on 16/08/2023

# Feature: Data loading functions and classes
# Implements the tools to load the data described in: https://www.kaggle.com/competitions/digit-recognizer/data
# The digitData class is composed of two attributes:
#   - an image (28x28 monochrome pixel colormap with values ranging between 0 and 255)
#   - a label (digit [0;9])

import csv


def check_image_data(array):
    """
    Checks if the array follows the format defined in the header
    :param array:
    :return: True if the format is valid, False otherwise
    """
    return len(array) == 784 or all(0 >= element >= 255 for element in array)


class DigitData:
    """
        This class contains the digit data

        Attributes:
            imageData (int): Monochromatic color-mapped data of size 28x28.
            label (int): Ground truth label for the digit illustrated in the imageData attribute. Ranges from 0 to 9.

        Methods:
            method1(param1, param2):
                A description of what this method does.

            method2(param):
                Another method description.
        """

    def __init__(self, image_data, label):
        """
        Initialize the class with imageData and label.

        :param image_data: (int array) Monochromatic color-mapped data of size 28x28.
        :param label: (int) Ground truth label for the digit illustrated in the imageData attribute.
                      Ranges from -1 to 9:
                        * -1 means that no label was defined (typically for the kaggle test data)
                        * 0 to 9 represent all digits that can be recognized
        :raises: ValueError if the image or label are not respecting the expected format.
        """
        if not check_image_data(image_data):
            raise ValueError("Invalid data format")
        elif not -1 <= label <= 9:
            raise ValueError("Wrong Label")
        else:
            self.imageData = image_data
            self.label = label


def create_digit_data_array_from_csv(file_path, is_test_data):
    """

    :param file_path: (str) Path to the CSV file.
    :param is_test_data: (bool) True if loading test data, false otherwise.
    :return: An array containing all digit data from csv file.
    :raises: ValueError (array<DigitData>) if the image or label are not respecting the expected format.
            FileNotFoundError if the csv file can't be opened due to wrong path.
            PermissionError if the csv file can't be opened due to permission restrictions.
    """
    data_array = []
    with open(file_path, 'r', newline='') as csv_file:

        try:
            csv_reader = csv.reader(csv_file)
            # Skip the header row
            next(csv_reader)
            if is_test_data:
                for row in csv_reader:
                    data_array.append(DigitData([int(pixelValue) for pixelValue in row[:]], -1))
            else:
                for row in csv_reader:
                    data_array.append(DigitData([int(pixelValue) for pixelValue in row[1:]], int(row[0])))

        except ValueError as e:
            raise ValueError(e)
        except FileNotFoundError as e:
            raise FileNotFoundError(e)
        except PermissionError as e:
            raise PermissionError(e)

    return data_array
