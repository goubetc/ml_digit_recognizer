#!/usr/bin/env python

# File: main.py
# Created by Claude Goubet on 16/08/2023

# Scenario: main file
# launches the program

import sys

from src.dataLoader import create_digit_data_array_from_csv

train_data_path = "data/train.csv"
test_data_path = "data/test.csv"

if __name__ == '__main__':

    """
     Loading data
    """

    try:
        print("Loading training data")
        train_data = create_digit_data_array_from_csv(train_data_path, False)
        print("Loading test data")
        test_data = create_digit_data_array_from_csv(test_data_path, True)
    except ValueError as e:
        print("Exception caught:", e)
        sys.exit(1)  # Exit with a non-zero exit code
    except FileNotFoundError as e:
        print("Exception caught:", e)
        sys.exit(1)  # Exit with a non-zero exit code
    except PermissionError as e:
        print("Exception caught:", e)
        sys.exit(1)  # Exit with a non-zero exit code
