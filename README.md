# ML_Digit_Recognizer

- [ ] This project regroups the source code for the Kaggle [Digit Recognizer challenge](https://www.kaggle.com/competitions/digit-recognizer). A description of the dataset can be found [here](https://www.kaggle.com/competitions/digit-recognizer/data)

- [ ] This is a Python implementation, using Pythorch library.

## Usage

```
tbd
```

## License
- [ ] GNU AFFERO GENERAL PUBLIC LICENSE
	Version 3, 19 November 2007

## Project status
- [ ] Active

## Git Workflow

- [ ] In this project we are using a **Feature And Develop Branches**. At this stage this project aims at being a small executable program with fast updates developed by a signe person. a Feature And Develop Branches approach seems the most fit to these requirements.