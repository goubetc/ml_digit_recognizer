#!/usr/bin/env python

# File: testDataImport.py
# Created by Claude Goubet on 16/08/2023

# Scenario: Test of the data import
# tests the images of the first and last data imported from dataset

import unittest
import os
from ..src.dataLoader import create_digit_data_array_from_csv, check_image_data, DigitData

testImage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 188, 255, 94, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 191, 250, 253, 93, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 123, 248, 253, 167, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 80, 247, 253, 208, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
             207, 253, 235, 77, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 54, 209, 253, 253, 88,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 93, 254, 253, 238, 170, 17, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 210, 254, 253, 159, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16, 209, 253, 254, 240, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 27, 253, 253, 254, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 20, 206, 254, 254, 198, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 168, 253,
             253, 196, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 203, 253, 248, 76, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 22, 188, 253, 245, 93, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 103, 253, 253, 191, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 89, 240, 253, 195, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 15, 220, 253, 253, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 94,
             253, 253, 253, 94, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 89, 251, 253, 250,
             131, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 214, 218, 95, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
testImageOutOfBound = testImage[:]
testImageOutOfBound[3] = 5000
testImageTooLong = testImage[:]
testImageTooLong.append(0)
testLabel = 1


class TestImageCheck(unittest.TestCase):
    def test_good_image_data(self):
        self.assertEqual(True, check_image_data(testImage))  # Test if image in right format returns True

    def test_too_small_image_data(self):
        self.assertEqual(False, check_image_data(testImage[5:]))  # Test if too small image returns False

    def test_too_long_image_data(self):
        self.assertEqual(False, check_image_data(testImageTooLong))  # Test if too long image returns False

    def test_out_of_bound_image_data(self):
        self.assertEqual(False, check_image_data(testImageTooLong))  # Test if out of bound value returns False


class TestExceptions(unittest.TestCase):
    def test_too_small_image_data(self):
        with self.assertRaises(ValueError):
            DigitData(testImage[5:], 1)  # Test if image with not enough number of pixels

    def test_too_long_image_data(self):
        with self.assertRaises(ValueError):
            DigitData(testImageTooLong, 1)  # Test if too long image returns False

    def test_out_of_bound_image_data(self):
        with self.assertRaises(ValueError):
            DigitData(testImageTooLong, 1)  # Test if out of bound value returns False

    def test_out_of_bound_label(self):
        with self.assertRaises(ValueError):
            DigitData(testImage, 200)  # Test if label out of bound

    def test_file_not_found_exception(self):
        with self.assertRaises(FileNotFoundError):
            create_digit_data_array_from_csv("randomPath", True)  # Test if wrong file path raises exception

    def test_file_forbidden_exception(self):
        with self.assertRaises(PermissionError):
            script_dir = os.path.dirname(__file__)
            csv_path = os.path.join(script_dir, "testData/goodImage_train.csv")
            # Get the current permissions of the file
            current_permissions = os.stat(csv_path).st_mode
            new_permissions = current_permissions & ~0o400
            # Apply the updated permissions to the file
            os.chmod(csv_path, new_permissions)
            try:
                create_digit_data_array_from_csv(csv_path, True)  # Test if access denied
            except PermissionError as e:
                # Add read permission back to the file
                new_permissions = current_permissions | 0o400
                # Apply the updated permissions to the file again
                os.chmod(csv_path, new_permissions)
                raise e
            # Add read permission back to the file
            new_permissions = current_permissions | 0o400
            # Apply the updated permissions to the file again
            os.chmod(csv_path, new_permissions)

class TestImageLoad(unittest.TestCase):
    def test_loading_good_csv_train_data_array(self):
        script_dir = os.path.dirname(__file__)
        csv_path = os.path.join(script_dir, "testData/goodImage_train.csv")
        one_csv_image = create_digit_data_array_from_csv(csv_path, False)
        self.assertEqual(testImage, one_csv_image[0].imageData)  # Checks if image pixel data correctly loaded

    def test_loading_good_csv_train_data_label(self):
        script_dir = os.path.dirname(__file__)
        csv_path = os.path.join(script_dir, "testData/goodImage_train.csv")
        one_csv_image = create_digit_data_array_from_csv(csv_path, False)
        self.assertEqual(testLabel, one_csv_image[0].label)  # Checks if image label correctly loaded

    def test_loading_good_csv_test_data_array(self):
        script_dir = os.path.dirname(__file__)
        csv_path = os.path.join(script_dir, "testData/goodImage_test.csv")
        one_csv_image = create_digit_data_array_from_csv(csv_path, True)
        self.assertEqual(testImage, one_csv_image[0].imageData)  # Checks if image pixel data correctly loaded

    def test_loading_good_csv_test_data_label(self):
        script_dir = os.path.dirname(__file__)
        csv_path = os.path.join(script_dir, "testData/goodImage_test.csv")
        one_csv_image = create_digit_data_array_from_csv(csv_path, True)
        self.assertEqual(-1, one_csv_image[0].label)  # Checks if image label correctly loaded


if __name__ == '__main__':
    unittest.main()
